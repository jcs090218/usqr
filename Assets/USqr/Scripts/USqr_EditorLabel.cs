#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace USqr.Core
{
    /// <summary>
    /// Display a label in the scene.
    /// </summary>
    public class USqr_SqrEditorLabel : MonoBehaviour
    {
        [Tooltip("Label text")]
        public string text = "<N/A>";

        [Tooltip("Offset the position.")]
        public Vector3 offset;

        [Tooltip("Always show on the screen.")]
        public bool alwaysShow = false;

        private static GUIStyle style;

        private static GUIStyle Style
        {
            get
            {
                if (style == null)
                {
                    style = new GUIStyle(EditorStyles.largeLabel);
                    style.alignment = TextAnchor.MiddleCenter;
                    style.normal.textColor = new Color(0.9f, 0.9f, 0.9f);
                    style.fontSize = 32;
                }
                return style;
            }
        }

        private void OnDrawGizmos()
        {
            RaycastHit hit;
            Ray r = new Ray(transform.position + Camera.current.transform.up * 8f, -Camera.current.transform.up);

            if (GetComponent<Collider>().Raycast(r, out hit, Mathf.Infinity))
            {
                float dist = (Camera.current.transform.position - hit.point).magnitude;

                float fontSize = Mathf.Lerp(64, 12, dist / 10f);

                Style.fontSize = (int)fontSize;

                Vector3 wPos = hit.point + Camera.current.transform.up * dist * 0.07f;

                Vector3 scPos = Camera.current.WorldToScreenPoint(wPos);
                if (scPos.z <= 0)
                {
                    return;
                }

                float alpha = 1.0f;

                if (!alwaysShow)
                {
                    alpha = Mathf.Clamp(-Camera.current.transform.forward.y, 0f, 1f);
                    alpha = 1f - ((1f - alpha) * (1f - alpha));

                    alpha = Mathf.Lerp(-0.2f, 1f, alpha);
                }

                Handles.BeginGUI();

                scPos.y = Screen.height - scPos.y;  // Flip Y

                Vector2 strSize = Style.CalcSize(new GUIContent(text));

                Rect rect = new Rect(0f, 0f, strSize.x + 6, strSize.y + 4);
                rect.center = scPos - Vector3.up * rect.height * 0.5f - offset;
                GUI.color = new Color(0f, 0f, 0f, 0.8f * alpha);
                GUI.DrawTexture(rect, EditorGUIUtility.whiteTexture);
                GUI.color = Color.white;
                GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
                GUI.Label(rect, text, Style);
                GUI.color = Color.white;

                Handles.EndGUI();
            }
        }
    }
}
#endif
