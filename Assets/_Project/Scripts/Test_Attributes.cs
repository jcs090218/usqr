using UnityEngine;
using MyBox;

public class Test_Attributes : MonoBehaviour
{
    [MinMaxRange(0, 1)]
    public RangedFloat raito;

#if UNITY_EDITOR
    [ButtonMethod]
    private void Print()
    {
        MyDebug.LogColor(Color.white);
    }
#endif
}
