[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)
[![Unity Engine](https://img.shields.io/badge/unity-2023.1.0f1-black.svg?style=flat&logo=unity)](https://unity3d.com/get-unity/download/archive)
[![Latest Release](https://gitlab.com/jcs090218/usqr/-/badges/release.svg)](https://gitlab.com/jcs090218/usqr/-/releases)

# USqr

[![pipeline status](https://gitlab.com/jcs090218/usqr/badges/main/pipeline.svg)](https://gitlab.com/jcs090218/usqr/-/commits/main)
[![coverage report](https://gitlab.com/jcs090218/usqr/badges/main/coverage.svg)](https://gitlab.com/jcs090218/usqr/-/commits/main)

This is a mixture of many tools but focuses on lightweight and performance.

## 📦 Upstream

- [MyBox](https://github.com/Deadcows/MyBox) by `@Deadcows`
- [ShaderForge](https://github.com/FreyaHolmer/ShaderForge) by `@FreyaHolmer`
- [PriorityQueue](https://github.com/FyiurAmron/PriorityQueue/blob/main/PriorityQueue.cs) by `@microsoft`
